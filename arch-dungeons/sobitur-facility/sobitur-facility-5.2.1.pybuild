# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1, InstallDir, File
from pybuild.modinfo import M
from pyclass import CleanPlugin, TRPatcher, CLEAN_DEPEND, TR_PATCHER_DEPEND


class Mod(CleanPlugin, TRPatcher, Pybuild1):
    NAME = "Sobitur Facility"
    DESC = "Adds an operational Dwemer Cyborg factory to the north of Vos"
    HOMEPAGE = "http://mw.modhistory.com/download--13780"
    # Original content is attribution-distribution,
    # but incorporated mods have other licenses
    LICENSE = "attribution-distribution all-rights-reserved CC-BY-SA-1.0"
    RESTRICT = "mirror"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        tr? ( landmasses/tamriel-data )
    """
    # TODO: Conflicts with (integrated)
    # TODO: De-bundle dependencies
    # "Underwater Dwemer Ruins",
    # "Re-coloured Ebony and Dwemer"
    # "Dwemer Cyborgs"
    # "Dwemer_Bow_and_Helmet"
    # "Dwemer Golem"
    # "Dwemer Sentinels"
    # TODO: Compatibility patch for yagrum advanced
    # RDEPEND = yagrum-advanced? ( yagrum-advanced gameplay/dwemer-alchemy )
    DEPEND = f"""
        base/morrowind
        {TR_PATCHER_DEPEND}
        {CLEAN_DEPEND}
    """
    # DEPEND = yagrum-advanced? ( gameplay/dwemer-alchemy )
    KEYWORDS = "openmw"
    SRC_URI = f"http://mw.modhistory.com/file.php?id=13780 -> {M}.rar"
    # SRC_URI =
    # https://s3-us-west-2.amazonaws.com/modhistory/morrowind/additional/DA_Sobitur_YA_Compatability.rar
    TEXTURE_SIZES = "256"
    IUSE = "tr"
    # IUSE = yagrum-advanced
    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[
                File("DA_Sobitur_TRIngred_Compat.ESP", REQUIRED_USE="tr"),
                File("DA_Sobitur_Quest_Part_2 Clean.esp"),
                File("DA_Sobitur_Facility_Clean.ESP"),
                File("DA_Sobitur_Quest_Part_1 Clean.esp"),
                File("DA_Sobitur_YA_Compatibility.ESP"),
                File("DA_Sobitur_Repurposed_1.ESP"),
            ],
            S=f"{M}",
        ),
        # InstallDir(
        #    "Data Files",
        #    PLUGINS=[File("DA_Sobitur_YA_Compatibility.ESP")],
        #    SOURCE="DA_Sobityr_YA_Compatability",
        #    REQUIRED_USE="yagrum-advanced",
        # ),
    ]

    def src_prepare(self):
        self.tr_patcher("DA_Sobitur_TRIngred_Compat.ESP")
        self.clean_plugin("DA_Sobitur_Facility_Clean.ESP")
        self.clean_plugin("DA_Sobitur_Quest_Part_1 Clean.esp")
        self.clean_plugin("DA_Sobitur_YA_Compatibility.ESP")
        self.clean_plugin("DA_Sobitur_Repurposed_1.ESP")
        # self.clean_plugin(
        #    os.path.join(
        #        self.WORKDIR,
        #        "DA_Sobitur_YA_Compatability",
        #        "Data Files",
        #        "DA_Sobitur_YA_Compatibility.ESP",
        #    )
        # )
