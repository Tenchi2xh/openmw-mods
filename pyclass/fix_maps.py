# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import re
import os
from typing import Optional
from pybuild import Pybuild1


class FixMaps(Pybuild1):
    """
    Removes references to textures from meshes
    and renames textures to match the openmw convention

    Normal map: _n
    Normal height map: _nh
    Specular map: _spec
    Terrain specular map: _diffusespec

    Patterns should not include the dds file extension, and should include
    a leading underscore '_', but can be regular expressions.
    """

    NORMAL_MAP_PATTERN: Optional[str] = None
    NORMAL_HEIGHT_MAP_PATTERN: Optional[str] = None
    SPECULAR_MAP_PATTERN: Optional[str] = None
    TERRAIN_SPECULAR_MAP_PATTERN: Optional[str] = None

    def __init__(self):
        self.DEPEND = self.DEPEND + " bin/openmw-nif-cleaner"

    def rename(path: str, old: str, new: str):
        new_path = re.sub(rf"{old}\.dds$", f"{new}.dds$", path, flags=re.IGNORECASE)
        print(f'Renaming "{path}" -> "{new_path}"')
        os.rename(path, new_path)

    def src_prepare(self):
        super().src_prepare()
        for root, dirs, files in os.walk(self.WORKDIR):
            for file in files:
                name, ext = os.path.splitext(file)
                if ext.lower() == ".dds":
                    for old, new in [
                        (self.NORMAL_MAP_PATTERN, "_n"),
                        (self.NORMAL_HEIGHT_MAP_PATTERN, "_nh"),
                        (self.SPECULAR_MAP_PATTERN, "_spec"),
                        (self.TERRAIN_SPECULAR_MAP_PATTERN, "_spec"),
                    ]:
                        if old is not None and re.match(
                            f".*{old}$", name, flags=re.IGNORECASE
                        ):
                            FixMaps.rename(os.path.join(root, file), old, new)

        print("Scanning for meshes to fix...")
        self.execute("openmw-nif-cleaner .")
